import { Router } from "express";
import * as  PacientesRoutes from "./pacientes/routes";

export function init() {
    const router = Router();
    PacientesRoutes.init(router);

    return router;
}