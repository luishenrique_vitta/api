import cookieParser from "cookie-parser";
import createError from "http-errors";
import express from "express";
import helmet from "helmet";
import morgan from "morgan";
import path from "path";
import cors from"cors";

import { Logger } from "./shared/services/Logger";
import * as Router from "./router";
import { NotFoundError } from "./shared/errors/NotFoundError";
import { Authentication } from "./shared/middlewares/Authentication";
import { UnauthenticatedError } from "./shared/errors/UnauthenticatedError";

require('./pacientes/boot');

const app = express();

app.use(morgan("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "../public")));

app.use(cors());

app.use(helmet());

app.use(Authentication);

app.use("/", Router.init());

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(new NotFoundError("PAGE NOT FOUND"));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  Logger.error(err);

  let status = 500;
  let error = "SERVER ERROR";

  if (err instanceof NotFoundError) {
    status = 404;
    error = "NOT FOUND";
  }

  if (err instanceof UnauthenticatedError) {
    status = 401;
    error = err.message || "UNAUTHENTICATED";
  }

  // render the error page
  return res.status(status).json({ error });
});


Logger.info("Ready");
export { app };
