import { PacientesRepository } from "../repositories/PacientesRepository";
import { IPaciente } from "../repositories/models/Paciente";

export class PacienteDomain {
    async listar() : Promise<IPaciente[]> {
        return new PacientesRepository().listar();
    }
}