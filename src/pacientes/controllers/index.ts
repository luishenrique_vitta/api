import { Response, Request, NextFunction } from "express";
import { PacienteDomain } from "../domain/PacienteDomain";

export const PacientesController = {
    async index(request: Request, response: Response, next: NextFunction) {
        response.json({ data: await new PacienteDomain().listar() });
    }
}