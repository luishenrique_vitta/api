import { Router, Handler } from "express";
import { PacientesController } from "./controllers";

export function init(router: Router) {
    router.get("/pacientes", PacientesController.index);
}