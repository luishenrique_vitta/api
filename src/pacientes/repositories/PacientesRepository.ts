import { IRepository } from "../../shared/repositories/IRepository";
import { IPaciente } from "./models/Paciente";

export class PacientesRepository extends IRepository {
    getModelName(): string {
        return "Paciente";
    }

    async listar(): Promise<IPaciente[]> {
        return [
            { id: 1, nome: "Luís Henrique" }
        ];
    }
}