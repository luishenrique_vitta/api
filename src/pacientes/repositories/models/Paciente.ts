import { TEXT, STRING, INTEGER, DATE, ENUM, Instance } from "sequelize";
import { sequelize } from "../../../shared/services/sequelize";

export const Paciente = sequelize.define('Paciente', {
    id: {
        type: INTEGER(11).UNSIGNED,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true,
    },

    nome: {
        type: STRING(120),
        allowNull: false,
    },
}, {
    tableName: "paciente",
});

export interface IPaciente {
    id: number
    nome: string
}