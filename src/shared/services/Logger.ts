import { createLogger, format, transports } from "winston";


export const Logger = createLogger({
    level: 'debug',
    format: format.prettyPrint(),
    transports: [
        new transports.Console({
            level: "debug"
        })
    ]
});