import { Transaction, Model, Instance, WhereOptions } from "sequelize";
import { RepositoryError } from "../errors/RepositoryError";
import { pick, omit } from "lodash";
import { ObjectNotFoundError } from "../errors/ObjectNotFoundError";
import { sequelize } from "../services/sequelize";

export abstract class IRepository {
    protected transaction: Transaction = null;

    constructor(transaction: Transaction = null) {
        if (transaction) this.setTransaction(transaction);
    }

    setTransaction(transaction: Transaction): this {
        this.transaction = transaction;
        return this;
    }

    getTransaction(): Transaction {
        return this.transaction;
    }

    async exists(id: string | number) : Promise<boolean> {
        if (!id) {
            return false;
        }

        // @todo use count
        return !! await this.getModel().findById(id);
    }

    async getOrStartTransaction(): Promise<GetOrStartTransactionResponse> {
        const alreadyHasTransaction = this.transaction != null;
        const transaction = alreadyHasTransaction ? this.transaction : await sequelize.transaction();

        return { transaction, isANewTransaction: !alreadyHasTransaction };
    }

    getModel(): Model<any, any> {
        const modelName = this.getModelName();
        const model = sequelize.models[modelName];
        if (!model) {
            throw new RepositoryError(`Model ${modelName} not found.`);
        }

        return model;
    }

    async findById(id: string | number, include: Array<string> = []): Promise<Object> {
        const res = await this.getModel().findById(id, { include, transaction: this.getTransaction() });
        if (res) {
            return res.toJSON();
        }

        return res;
    }

    async findAll(conditions: WhereOptions<any>, attributes: string[] = null): Promise<object[]> {
        const records = await this.getModel().findAll({
            where: conditions,
            transaction: this.getTransaction(),
            attributes,
        });

        return records.map(record => record.toJSON());
    }

    async destroyAll(conditions: WhereOptions<any>): Promise<Number> {
        this.validateConditions(conditions);

        return this.getModel().destroy({
            where: conditions,
            transaction: this.getTransaction(),
        });
    }

    async updateByCondition(conditions: WhereOptions<any>, data: object): Promise<object | boolean> {
        this.validateConditions(conditions);

        const persistenceData = { ...data };

        return this.getModel().update(persistenceData, {
            where: conditions,
            transaction: this.getTransaction(),
        });
    }

    async store(data: object): Promise<object> {
        const record = await this.getModel()
            .create(data, { transaction: this.getTransaction() });

        return record.toJSON();
    }

    async update(id: number | string, data: object): Promise<object> {
        let transaction, isANewTransaction;

        try {
            ({ transaction, isANewTransaction } = await this.getOrStartTransaction());

            const originalRecord = await this.getModel().findById(id);
            if (!originalRecord) {
                throw new RecordNotFoundError('Record not found.');
            }

            const newRecord = await originalRecord.update(data, { transaction });

            if (isANewTransaction) {
                await transaction.commit();
            }

            return newRecord.toJSON();
        } catch (e) {
            transaction && isANewTransaction && await transaction.rollback();
            throw e;
        }
    }

    async destroy(id: number | string): Promise<boolean> {
        let transaction;
        let isANewTransaction;

        try {
            ({ transaction, isANewTransaction } = await this.getOrStartTransaction());

            const record = await this.getModel().findById(id);
            if (!record) {
                throw new RecordNotFoundError('Record not found.');
            }

            await record.destroy({ transaction });

            if (isANewTransaction) {
                await transaction.commit();
            }

            return true;
        } catch (e) {
            transaction && isANewTransaction && await transaction.rollback();
            throw e;
        }
    }

    async search(query: ISearchQuery, page: number = null, pageSize: number = 10, count: boolean = false): Promise<ISearchResult> {
        const transaction = this.getTransaction();
        const where = this.extractConditionsFromSearchQuery(query);
        const order = this.extractOrderFromSearchQuery(query);
        const attributes = this.extractAttributesFromSearchQuery(query);
        const { limit, offset } = this.translatePageAndSizeToLimitAndOffset(page, pageSize);
        const include = this.extractIncludeFromSearchQuery(query);
        const paranoid = true;

        const promises = [
            this.getModel().findAll(
                { transaction, where, order, limit, offset, include, attributes, paranoid }
            ),
        ];

        if (count === true) {
            promises.push(this.getModel().count({ transaction, where }));
        }

        const [records, total] = await Promise.all(promises);
        return {
            total: count ? total : null,
            page,
            pageSize,
            pages: count ? Math.ceil(total / pageSize) : null,
            data: records.map(record => record.toJSON()),
        };
    }

    private translatePageAndSizeToLimitAndOffset(page: number, pageSize: number): { limit: number | null; offset: number | null; } {
        return {
            limit: page ? pageSize : null,
            offset: page ? pageSize * (page - 1) : null,
        };
    }

    extractOrderFromSearchQuery(query: ISearchQuery): Array<ISearchQuerySort> {
        if (!query.sort || query.sort.length === 0) return [];

        const res = [];
        query.sort.forEach(sort => {
            const field = sort.field;
            const direction = sort.direction || 'ASC';

            res.push({ field, direction });
        });

        return res;
    }

    extractConditionsFromSearchQuery(query: ISearchQuery): object {
        const res = pick(query.filters || {}, Object.keys(this.getModel().attributes));
        if (!query.q) {
            return res;
        }

        const modelAttibutes = this.getModel().attributes;
        const searchableFields = Object.keys(modelAttibutes)
            .reduce((fields, attribute) => {
                if (modelAttibutes[attribute].searchable === true) {
                    fields.push(Object.assign({ name: attribute }, modelAttibutes[attribute]));
                }

                return fields;
            }, []);

        if (searchableFields.length === 1 && searchableFields[0].name in res === false) {
            res[searchableFields[0].name] = { $like: `%${query.q}%` };
        } else if (searchableFields.length > 0) {
            res.$or = searchableFields.map(field => ({
                [field.name]: { $like: `%${query.q}%` },
            }));
        } else {
            throw new RepositoryError('Text search is not supported.');
        }

        return res;
    }

    extractIncludeFromSearchQuery(query: ISearchQuery) {
        return (query.include || []).map((include) => {
            if (typeof include === 'string') {
                if (include in this.getModel().associations === true) {
                    return { model: this.getModel().associations[include].target, as: include };
                }

                return { model: sequelize.models[include] };
            }

            return pick(include, ['model', 'as', 'where', 'include']);
        });
    }

    extractAttributesFromSearchQuery(query: ISearchQuery) {
        if (query.attributes.length === 0) {
            return null;
        }

        const getModelName = model => model.name;

        return query.attributes.map((attribute) => {
            const attributeSplitted = attribute.split('.');

            if (attributeSplitted.length === 1) {
                return attribute;
            }

            // @todo support relationships
        });
    }

    validateConditions(conditions: WhereOptions<any>) {
        const conditionKeys = Object.keys(conditions);
        const modelKeys = Object.keys(this.getModel().attributes);

        if (conditionKeys.length === 0) {
            throw new RepositoryError('At least one filter is required');
        }

        const hasValidConditions = conditionKeys.some(
            key => modelKeys.includes(key) && !!conditions[key]
        );
        if (!hasValidConditions) {
            throw new RepositoryError('At leat one filter must be vaid.');
        }
    }

    abstract getModelName(): string;
};
