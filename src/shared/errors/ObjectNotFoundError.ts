/**
 * OBJECT NOT FOUND
 */
export class ObjectNotFoundError extends Error {
    public name = "RecordNotFoundError";
}