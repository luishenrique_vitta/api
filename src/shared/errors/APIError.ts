/**
 * HTTP ERROR
 */
export class APIError extends Error {
    status: number;

    constructor(message, status = 500) {
        super(message);
        this.status = status;
    }
}