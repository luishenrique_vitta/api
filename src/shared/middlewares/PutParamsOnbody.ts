import { Request, Response, NextFunction, Handler } from 'express';

export function handler(params: Array<string> = null) {
    return (request: Request, response: Response, next: NextFunction) => {
        if (["PUT", "POST"].includes(request.method) === false) {
            return next();
        }

        const keys = params === null ? Object.keys(request.params) : params;

        keys.forEach(key => {
            if (key in request.params === true) {
                request.body[key] = request.params[key];
            }
        });

        return next();
    }
}

export function TransformParameterInFilter(params: Array<string> = null) : Handler {
    return (request: Request, response: Response, next: NextFunction) => {
        if (request.method !== "GET") return next();

        const keys = params === null ? Object.keys(request.params) : params;

        keys.forEach(key => {
            if (key in request.params === true) {
                request.query[key] = request.params[key];
            }
        });

        return next();
    }
}