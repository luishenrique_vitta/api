import { pick, omit } from 'lodash';
import { Request, Response, NextFunction } from 'express';

const getAsArray = (source, key) => {
    if (key in source === true) {
        return Array.isArray(source[key]) ? source[key] : [source[key]];
    }

    return [];
};

export function handler(defaults: { page?: number, pageSize?: number, q?: string } = {}) {
    return (request: Request, response: Response, next: NextFunction) => {
        const args = {
            page: request.query.$page ? parseInt(request.query.$page, 10) : defaults.page,
            pageSize: request.query.$pageSize ? parseInt(request.query.$pageSize, 10) : defaults.pageSize,
            q: (request.query.q || (defaults.q || '')).trim(),
            filters: omit(request.query, ['$page', '$pageSize', '$sort', 'q']),
            include: getAsArray(request.query, '$include'),
            attributes: getAsArray(request.query, '$fields'),
            sort: getAsArray(request.query, '$sort')
                .filter(field => field)
                .map(sortString => ({
                    field: sortString.replace(/^\-+/, ''),
                    direction: sortString.startsWith('-') ? 'DESC' : 'ASC',
                })),
        };

        request.EXTRACTED_QUERY = args;
        next();
    }
}

export interface SearchQueryExtractorResponse {
    page: number;
    pageSize: number;
    q: String;
    filters: object;
    include: Array<string>;
    attributes: Array<string>;
    sort: Array<{field: String, direction: String}>
}