import { Request, Response, NextFunction } from "express";
import { UnauthenticatedError } from "../errors/UnauthenticatedError";

export function Authentication(request: Request, respone: Response, next: NextFunction) {
    const authHeader = request.header("Authorization");
    const accessTokenFromQuery = request.query.access_token;

    if (!authHeader && !accessTokenFromQuery) {
        return next(new UnauthenticatedError("Token de autorização não informado."));
    }

    let token;
    if (accessTokenFromQuery) {
        token = accessTokenFromQuery;
    } else {
        const [type, token] = authHeader.split(" ");
        if (!type || !token) {
            return next(new UnauthenticatedError("Token inválido."));
        }

        if (!["JWT", "Bearer"].includes(type)) {
            return next(new UnauthenticatedError("Token com tipo inválido."))
        }
    }

    return next();
}