Alergias
    Alergias
    AlergiasReferencia (lista de alergias conhecidas, mas sem vínculo com as alergias);

    AlertaPaciente (alertas?idPaciente=1)

Assinaturas (tem uma coluna PDF, cod_hist, utc)
? AutorizadorSolicitacao ()
? cidcap
? cidgrp
? CondutaMedica
? ControleLicencas
CurvaCrescimento
Diagnósticos
  DiagnósticosCompl
? DiretrizDiag
? Diretrizes

EvolucaoPrescricao

Customizacao/Configuracao
    EmpresaBotoesAtendimento
    EmpresaConfiguracoes
    BotoesAtendimento (se não me engano controla os botões que são exibidos no prontuário);
    ProfissionalBotoesAtendimento
    Menusite

Odontologia
    Odontogramas
    Anotações (aparentemente tem a ver com odontologia, tem código do odontograma);


Medicamentos
    Medicamentos
    MedicamentosCompl
    MedicamentoTipo


Protocolos?
    ProtAplicacoes
    ProtBlocoProtocolo
    ProtBlocos
    ProtItens
    ProtProtocolos
    ProtQuestaoBloco
    ProtQuestoes
    ProtQuestoesTipo
    ProtRespostas
    ProtSolicitacoes

Exames?
    Exames
    ExamesCompl
    ExamesFaixas
    ExamesFormulaCalculo
    ExamesGrupos
    ExamesUnidades
    GrupoExames (ExameGrupos)
    Resultados
    ResultadosComplemento
    LaboratorioExame
    LaboratorioProfissional
    LaudoImagem
    Audiometria (Jesuis!)
    GrupoResultados
    GraficoExames
    GraficoExamesDataset


Oftamlologia
    ExameOftalmo
    ExameOftalmoBiometria
    ExameOftalmoCtd
    LaudoOftalmo
    LentoContato
    LenteExame
    LenteExamePreConsulta
    LenteOftalmo
    LentePedido
    LentePreConsulta
    MarcaLente (LenteMarca)
    MarcaLenteCompl (LenteMarcaComplemento)
    TipoExameOftalmo


Historias
    HistAutorizacao
    HistCompartilhar
    HistDiagnóstico
    HistEuPrescrevo
    HistLog
    HistMedicamento
    HistNexodata
    HistProcedimento
    Historia
      HistoriaCompl
      HistoriaCondutaMedica
      HistoriaCompra
      TipoHistoria

Folha
Generos
? Ginecologia
  GinegolociaItens

GrauParentesco

Historicos
ImagemProfissional
Imagens

Lembretes
LembretesModelos


ModeloProcedimento


OncBaixa
OncCiclo
  OncCicloItem
  OncCicloProtocolo
OncFaturamento
OncInfusao
OncProtMaterial
OncProtMedicamento
OncProtocolo
OncRegraConvenio
OncTipoMed(icamento?)
OncValor

Pacientes
    PacienteRegistros
    PacienteTags

PadraoCurvasAltura
PadraoCurvasPeso
PlanosAtendimento
  PlanosAtencimentoAcoes
  PlanosAtendimentoAcoesVinculos
  PlanosAtendimentoProfissionais
  PlanosAtendimentoTags
  PlanosAtendimentoTipoAcoes
PreConsultaOftalmo
Prescricao
  PrescricaoItem
  PrescricaoItemEnfermaria
  PrescricaoItemFarmacia
  PrescricaoItemFarmaciaLote
  PrescricaoMaterial
  PrescricaoProtocolo
  PrescricaoProtocoloItem
ProcedimentosAnotacoes
ProcedimentosModelo
ProcedimentosOdonto

Receita
Refracao
RefracaoExame

Reproducao
    RepAntecedObstetrico
    RepCoito
    RepDados
    RepExamesComplConjuge
    RepExamesGerais
    RepExamesGeraisConjuge
    RepFertilizacaoVitro
    RepFet
    RepGeneticoConjuge
    RepGeneticosMulher
    RepHisteroscopia
    RepHisterossal
    RepHormoniosConjuge
    RepHormoniosMulher
    RepIdentificacao
    RepInseminacao
    RepMicrobiologia
    RepOutrosExamesConjuge
    RepOutrosExamesMulher
    RepProcedurologicos
    RepRm
    RepSoroConjuge
    RepSoroMulher
    RepTromboMulher
    RepUltrassonPelvica
    RepUsgTesticularConjuge


SedeFilial
SolicitacaoQuimioterapia
t_Apresentacao
t_ComercialDados
t_ComercialFarmaco
t_Farmaco
t_Laboratorio
t_Receita
t_Via
Tabelas
Testers
TipoSanguineo
TiposImagens
TiposImagensOrigem
Usuario
VacCalendario
VacHistoricoVacinas
  VacHistoricoVacinasItens
Veiculo
Videos