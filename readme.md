# APIPOC
API - POC

## Tecnologias
- [Nodejs](https://nodejs.org/en/);
- [Typescript](https://www.typescriptlang.org/);
- [Express](http://expressjs.com/);

## Executando
### Instale as dependências

```
sudo npm install
```

### Rodar app
Você pode rodar diretamente na sua máquina.

```
npm run dev
```

Ou num container Docker

```
docker-compose up
```

**Atenção**

Você precisa do MySQL rodando também. Se utilizar o docker o banco sobe junto. Se for rodar local, precisa ter a instância funcionando e passar algumas variáveis de ambiente para o app:

```
env DB_HOST=meubanco DB_USER=root DB_PASSWORD=root DB_NAME=api npm run dev
```

Para subir a estrutura do banco, importe o arquivo `database/000_CREATE_DB.sql`.


## Desenvolvendo
O código está sendo escrito com typescript, então vocẽ você cria e edita arquivos no diretório `src` e o typescript cuida do processo de copiá-los para o diretório dist. Execute em modo *watch*  para um fluxo simplificado:

```
tsc --watch
```

